
function displayWindow(ID,theURL,winName,width,height,scroll,offy,lex) { 
    var anchorname=ID;
    var window_width = width;
//    if (scroll == '0') { window_width = width-20; }
    var window_height = height;
    var scroll= scroll;
    var offy=offy;
    var lex=lex;
    if (anchorname =="window") {


	var x=0;
	var y=0;
	if (document.getElementById) {
		if (isNaN(window.screenX)) {
			x=-document.body.scrollLeft+window.screenLeft+(800-window_width)/2;
			y=-document.body.scrollTop+window.screenTop+(600-window_height)/2;
			}
		else {
			x=window.screenX+(window.outerWidth-window.innerWidth)-window.pageXOffset+(800-window_width)/2;
			y=window.screenY+(window.outerHeight-24-window.innerHeight)-window.pageYOffset+(600-window_height)/2;
			}
		}
	else if (document.all) {
		x=-document.body.scrollLeft+window.screenLeft+(800-window_width)/2;
		y=-document.body.scrollTop+window.screenTop+(600-window_height)/2;
		}
	else if (document.layers) {
		x=window.screenX+(window.outerWidth-window.innerWidth)-window.pageXOffset+(800-window_width)/2;
		y=window.screenY+(window.outerHeight-24-window.innerHeight)-window.pageYOffset+(600-window_height)/2;
		}
    var window_top = y;
    var window_left = x;
    }
    else {
    var coordinates=getAnchorWindowPosition(anchorname);
    var y=0;
    var x=0;
    if (!offy) {y=50;}
    if (offy=="d") {y=35;
     x=150;}
    if (offy=="u") {y=-130;
     x=150;}
    var window_top=coordinates.y+y;
    var window_left=coordinates.x+x;
    }

    if ((!lex=='')) {theURL=theURL + '?' + lex;}
  if (anchorname =="window") {
  var features=('width='+'' +window_width+ ''+',height='+'' +window_height+ ''+',top='+'' +window_top+ ''+',left='+'' +window_left+ ''+',scrollbars='+'' +scroll+ ''+',resizable=no,status=no')  
      newWindow=window.open(''+ theURL + '',''+ winName + '',features);
newWindow.focus();
}
else {
    newWindow=window.open(''+ theURL + '',''+ winName + '','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',resizable=no,scrollbars=' + scroll + ''); newWindow.focus();
}
}


function getAnchorPosition(anchorname) {
	// This function will return an Object with x and y properties
	var useWindow=false;
	var coordinates=new Object();
	var x=0,y=0;
	// Browser capability sniffing
	var use_gebi=false, use_css=false, use_layers=false;
	if (document.getElementById) { use_gebi=true; }
	else if (document.all) { use_css=true; }
	else if (document.layers) { use_layers=true; }
	// Logic to find position
 	if (use_gebi && document.all) {
		x=AnchorPosition_getPageOffsetLeft(document.all[anchorname]);
		y=AnchorPosition_getPageOffsetTop(document.all[anchorname]);
		}
	else if (use_gebi) {
		var o=document.getElementById(anchorname);
		x=AnchorPosition_getPageOffsetLeft(o);
		y=AnchorPosition_getPageOffsetTop(o);
		}
 	else if (use_css) {
		x=AnchorPosition_getPageOffsetLeft(document.all[anchorname]);
		y=AnchorPosition_getPageOffsetTop(document.all[anchorname]);
		}
	else if (use_layers) {
		var found=0;
		for (var i=0; i<document.anchors.length; i++) {
			if (document.anchors[i].name==anchorname) { found=1; break; }
			}
		if (found==0) {
			coordinates.x=0; coordinates.y=0; return coordinates;
			}
		x=document.anchors[i].x;
		y=document.anchors[i].y;
		}
	else {
		coordinates.x=0; coordinates.y=0; return coordinates;
		}
	coordinates.x=x;
	coordinates.y=y;
	return coordinates;
	}



function getAnchorWindowPosition(anchorname) {
	var coordinates=getAnchorPosition(anchorname);
	var x=0;
	var y=0;
	if (document.getElementById) {
		if (isNaN(window.screenX)) {
			x=coordinates.x-document.body.scrollLeft+window.screenLeft;
			y=coordinates.y-document.body.scrollTop+window.screenTop;
			}
		else {
			x=coordinates.x+window.screenX+(window.outerWidth-window.innerWidth)-window.pageXOffset;
			y=coordinates.y+window.screenY+(window.outerHeight-24-window.innerHeight)-window.pageYOffset;
			}
		}
	else if (document.all) {
		x=coordinates.x-document.body.scrollLeft+window.screenLeft;
		y=coordinates.y-document.body.scrollTop+window.screenTop;
		}
	else if (document.layers) {
		x=coordinates.x+window.screenX+(window.outerWidth-window.innerWidth)-window.pageXOffset;
		y=coordinates.y+window.screenY+(window.outerHeight-24-window.innerHeight)-window.pageYOffset;
		}
	coordinates.x=x;
	coordinates.y=y;
	return coordinates;
	}

// Functions for IE to get position of an object
function AnchorPosition_getPageOffsetLeft (el) {
	var ol=el.offsetLeft;
	while ((el=el.offsetParent) != null) { ol += el.offsetLeft; }
	return ol;
	}

function AnchorPosition_getPageOffsetTop (el) {
	var ot=el.offsetTop;
	while((el=el.offsetParent) != null) { ot += el.offsetTop; }
	return ot;
	}
